<?php
/*
 * This file has been generated automatically.
 * Please change the configuration for correct use deploy.
 */

require 'recipe/common.php';

// Set configurations
env('application_name', 'simpah-api-totaljs');
set('repository', 'git@bitbucket.org:cristiantorres/simpah-api-totaljs.git');
set('shared_files', []);
set('shared_dirs', ['assets', 'configs']);
set('writable_dirs', []);

// Configure servers
server('production', 'simpah-api.premperhn.com')
    ->user('deploy')
    ->password()
    ->env('deploy_path', '/srv/simpah-api-totaljs');

server('development', 'simpah-api.premperhn.com')
    ->user('deploy')
    ->password()
	->env('branch', 'development')
    ->env('deploy_path', '/srv/simpah-api-totaljs');


/**
 * Install npm packages.
 */
task('deploy:npm', function () {
    $releases = env('releases_list');

    if (isset($releases[1])) {
        if(run("if [ -d {{deploy_path}}/releases/{$releases[1]}/node_modules ]; then echo 'true'; fi")->toBool()) {
            run("cp --recursive {{deploy_path}}/releases/{$releases[1]}/node_modules {{release_path}}");
        }
    }

    run("cd {{release_path}} && npm install");
});

/**
 * Restart server on success deploy.
 */
task('pm2:restart', function () {
    run("pm2 restart {{application_name}}");
})->desc('Restart pm2 service');

after('success', 'pm2:restart');

/**
 * Main task
 */
task('deploy', [
    'deploy:prepare',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:npm',
    'deploy:symlink',
    'cleanup',
])->desc('Deploy your project');

after('deploy', 'success');