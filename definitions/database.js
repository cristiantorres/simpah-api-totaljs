// Below code rewrites total.js database prototype
var dbtype = CONFIG('database.type') || 'mysql';
var dbuser = CONFIG('database.user') || 'root';
var dbpass = CONFIG('database.pass') || '';
var dbname = CONFIG('database.name') || 'mysql';
var dbhost = CONFIG('database.host') || 'localhost';
var dbport = CONFIG('database.port') || '3306';
var connection_string = dbtype + '://' + dbuser + ':' + dbpass + '@' + dbhost + ':' + dbport + '/' + dbname;
require('sqlagent/' + dbtype).init(connection_string, [false]); // debug is by default: false

// When you use RDMBS:
// var sql = DATABASE([ErrorBuilder]);
global.SQL = DATABASE();
// sql === SqlAgent