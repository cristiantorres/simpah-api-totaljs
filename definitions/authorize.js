// F.onAuthorize = function (req, res, flags, callback) {
//     var cookie = req.cookie(F.config.cookie);
//     if (!cookie || cookie.length < 10) {
//         callback(false);
//         return;
//     }
//
//     var obj = F.decrypt(cookie, 'user');
//
//     if (!obj || obj.ip !== req.ip) {
//         callback(false);
//         return;
//     }
//
//     var user = F.cache.read('user_' + obj.id);
//     if (user) {
//         req.user = user;
//         callback(true);
//         return;
//     }
//
//     SQL.select('user', 'users').make(function(builder) {
//         builder.where('id', obj.id);
//         builder.first();
//         builder.callback(function(err, response) {
//             if (!response)
//                 return callback(false);
//             F.cache.add('user_' + response.id, response, '5 minutes');
//             callback(true, response);
//         });
//     });
// };

F.on('module#auth', function (type, name) {
    var auth = MODULE('auth');
    auth.onAuthorize = function (id, callback, flags) {

        // - this function is cached
        if (id) {
            // - here you have to read user information from a database
            // - insert the user object into the callback (this object will be saved to session/cache)
            console.log('authorizing apikey ' + id);
            callback({id: id})
        } else {
            // if user not exist then
            callback(null);
        }
    };
});
