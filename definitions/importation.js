F.on('service', function (counter) {
    console.log('Importing data... ');
    counter % 1 === 0 && ['markets'].forEach(function (table) {
        var filename = framework.path.temporary(table + '.csv');
        SQL.query('Importation', "LOAD DATA INFILE '" + filename + "' INTO TABLE " + table + " FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\n' IGNORE 1 ROWS")
        SQL.exec(function (err, results) {
            if (err) return console.log(err);
            console.log('Data imported!');
        });
    })
});