exports.install = function () {

    // Sets cors for this API
    F.cors('/rest/*', ['get', 'post', 'put', 'delete'], true);

    F.route('/rest/{/login$/}', json_login, ['unauthorize']);
    F.route('/rest/{/logout$/}', json_logout, ['authorize']);

    // Creates routes
    F.restful('/rest/{model}/', CONFIG('controllers.rest.flags'), json_query, json_read, json_save, json_delete);

    F.on('controller', function (self, name) {
        if (name == 'rest') {
            var model = self.req.path[1].capitalize();
            self.singular = INFLECTOR(model, 1);
            self.plural = INFLECTOR(model, 2);
            self.page = self.query.page || 1;
            delete self.query.page;
            self.subscribe.route.schema = ['default', self.singular];
        }
    });
};

function json_login() {
    var self = this;
    var auth = MODULE('auth');
    var api_key = self.query.api_key || null;
    SQL.clear().select('ApiKey', 'ApiKeys').make(function (builder) {
        builder.where('ApiKey', api_key);
        builder.fields('ApiKey', 'ValidUntil')
    });
    SQL.exec(function (err, results) {
        if (typeof (results) !== 'undefined' && results['ApiKey'].length == 0) return self.res.send(401);
        var cb = self.callback();
        if (err) cb(err);
        else {
            auth.login(self, results['ApiKey'][0].ApiKey, results['ApiKey'][0]);
            cb(err, results);
        }
    });
}

function json_logout() {
    var self = this;
    MODULE('auth').logoff(self, 1);
    self.json({ message: 'Logout successful!' })
}

function json_query(model) {
    var self = this;
    SQL.clear().select(self.singular, self.plural).make(function (builder) {
        for (var key in self.query) {
            if (self.query.hasOwnProperty(key) && key.contains(':'))
                builder.like(key.split(':')[0], self.query[key], key.split(':')[1]);
            else
                builder.where(key, self.query[key]);
        }
        builder.page(self.page, 15);
    });
    SQL.exec(function(err, results){
        self.callback()(err, results[self.singular])
    });
}

function json_read(model, id) {
    var self = this;
    SQL.clear().select(self.singular, self.plural).make(function (builder) {
        // builder.like(self.singular + 'ID', id, '*')
        builder.where(self.singular + 'ID', id);
        builder.page(self.page, 15);
    });
    SQL.exec(function(err, results){
        self.callback()(err, results[self.singular])
    });
}

function json_create(model) {
    var self = this;
    self.$save(self.callback);
}

function json_save(model, id) {
    var self = this;
    SQL.clear().select(self.singular, self.plural).make(function (builder) {
        builder.where(self.singular + 'ID', id);
        builder.page(self.page, 15);
    });
    SQL.exec(function(err, results){
        self.callback()(err, results[self.singular])
    });
}

function json_delete(model, id) {
    var self = this;
    SQL.clear().select(self.singular, self.plural).make(function (builder) {
        builder.where(self.singular + 'ID', id);
        builder.page(self.page, 15);
    });
    SQL.exec(function(err, results){
        self.callback()(err, results[self.singular])
    });
}