NEWSCHEMA('User').make(function (schema) {

	schema.define('ID', 'UID');
	schema.define('FirstName', 'Capitalize(30)', true);
	schema.define('Surname', 'Capitalize(30)', true);
	schema.define('Email', 'Capitalize(30)', true);
	schema.define('Password', 'Capitalize(30)', true);
	schema.define('Password', 'Capitalize(30)', true);

	schema.setSave(function (error, model, options, callback) {

		// Removes hidden properties of the SchemaBuilder
		var data = model.$clean();

		// Checks if the user exists
		if (!model.id) {
			data.id = Utils.GUID();
			data.datecreated = new Date();
			SQL.insert('user', 'users')
				.make(function (builder) {
					builder.set(data);
					builder.set('Created', new Date());
				})
				.exec(callback);
			return;
		}

		data.dateupdated = new Date();

		// We don't need to modify id
		delete data.id;

		SQL.update('user', 'users')
			.make(function (builder) {
				builder.where('UserID', model.id)
				builder.set(data);
				builder.set('Updated', new Date());
			})
			.exec(callback);
	});

	schema.setGet(function (error, model, options, callback) {
		SQL.select('user', 'users')
			.make(function (builder) {
				builder.where('UserID', options.id);
				builder.page(options.page, 10);
			})
			.exec(function (err, response) {
				callback(err, response)
			});
	});

	schema.setQuery(function (error, options, callback) {
		// Reads the user
		SQL.select('user', 'users')
			.make(function (builder) {
				if (options.search) {
					builder.or();
					builder.search('Name', search);
					builder.end();
				}
				builder.fields('UserID', 'Description1', 'Description2', 'DTUpdated');
				builder.page(options.page, 10);
			})
			.exec(function (err, response) {
				callback(err, response)
			});
	});

	schema.setRemove(function (error, options, callback) {
		// Removes the user
		SQL.remove('user', 'users')
			.make(function (builder) {builder.where('UserID', options.id);})
			.exec(function (err, response) {callback(err, response)});
	});
});