NEWSCHEMA('Market').make(function (schema) {

    schema.define('MarketID', 'UID');
    schema.define('Description1', 'Capitalize(30)', true);

    schema.setSave(function (error, model, options, callback) {

        // Removes hidden properties of the SchemaBuilder
        var data = model.$clean();

        // Checks if the user exists
        if (!model.id) {
            data.id = Utils.GUID();
            data.datecreated = new Date();
            SQL.insert('market', 'markets')
                .make(function (builder) {
                    builder.set(data);
                    builder.set('Created', new Date());
                });
            SQL.exec(callback);
            return;
        }

        data.dateupdated = new Date();

        // We don't need to modify id
        delete data.id;

        SQL.update('market', 'markets')
            .make(function (builder) {
                builder.where('MarketID', model.id)
                builder.set(data);
                builder.set('Updated', new Date());
            });
        SQL.exec(callback);
    });

    schema.setGet(function (error, model, options, callback) {
        SQL.select('market', 'markets')
            .make(function (builder) {
                builder.where('MarketID', options.id);
                builder.page(options.page, 10);
            });
        SQL.exec(callback);
    });

    schema.setQuery(function (error, options, callback) {
        // Reads the market
        SQL.select('market', 'markets')
            .make(function (builder) {
                if (options.search) {
                    builder.or();
                    builder.search('Description1', search);
                    builder.end();
                }
                builder.fields('MarketID', 'Description1', 'Description2', 'DTUpdated');
                builder.page(options.page, 10);
            });
        SQL.exec(callback);
    });

    schema.setRemove(function (error, options, callback) {
        // Removes the market
        SQL.remove('market', 'markets')
            .make(function (builder) {
                builder.where('MarketID', options.id);
            });
        SQL.exec(callback);
    });
});